# Cisco Facts Gathering Library
## Content

 - [About this repo](#about-this-repo)
 - [Capabilities](#capabilities)

## About this repo
This repository provides a simple set of tools for creating basic documentation of data networks with Cisco devices. It consist of connection modules for SSH and Telnet (using [Netmiko](https://github.com/ktbyers/netmiko) library) and parser modules, which turn the CLI output to JSON files for further processing.
## Capabilities
This project aims mainly at Cisco networking equipment, primarily Catalyst Switches and ISR Routers. Nexus series switches are partially supported. Other vendor's equipment might be added later.
