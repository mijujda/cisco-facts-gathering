from setuptools import setup

setup(
    name='cisco-facts-gathering',
    version='0.0.1',
    packages=['cisco_facts_gathering', 'cisco_facts_gathering.utils', 'cisco_facts_gathering.Parsers', 'cisco_facts_gathering.Writers',
              'cisco_facts_gathering.Examples', 'cisco_facts_gathering.Discovery', 'cisco_facts_gathering.Connection'],
    install_requires=[
        "netmiko"
    ],
    url='',
    license='',
    author='Mira',
    author_email='mijujda@gmail.com',
    description=''
)
