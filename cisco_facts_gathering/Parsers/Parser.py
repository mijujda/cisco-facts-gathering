from cisco_facts_gathering.utils import *
from cisco_facts_gathering.definitions import DATA_PATH
from cisco_facts_gathering.Parsers import Patterns
import json
import re
import time


class ParserModule(object):
    def __init__(self, device_type, DEBUG):
        self.device_type = device_type
        self.DEBUG = DEBUG
        self.logger = get_logger(name=f"ParserModule-{device_type}", DEBUG=DEBUG)
        self.patterns = Patterns(device_type=device_type).get_patterns()

    def level_zero(self, text, patterns):
        """
        This functions tries to find correct regex string and returns list
        :param text: Command output which should be parsed
        :param patterns: List of compiled regex patterns
        :return:
        """
        level_zero_outputs = []
        named_groups = []
        if not isinstance(patterns, list):
            patterns = list(patterns)
        for pattern in patterns:
            if not isinstance(pattern, re._pattern_type):
                self.logger.critical(msg=f"Level 0: The pattern in list is not compiled regex pattern!")
                continue
            for group_name in pattern.groupindex.keys():
                if isinstance(group_name, str) and group_name not in named_groups:
                    named_groups.append(group_name)
        self.logger.debug(msg=f"Level 0: Patterns contain named groups: {named_groups}")
        # Find match
        for pattern in patterns:
            if re.search(pattern=pattern, string=text):
                if len(named_groups) == 0:
                    level_zero_outputs = re.findall(pattern=pattern, string=text)
                    self.logger.debug(msg=f"Level 0: Found {len(level_zero_outputs)} matches without named groups.")
                    return level_zero_outputs
                else:
                    for m in re.finditer(pattern=pattern, string=text):
                        entry = {}
                        for group_name in named_groups:
                            try:
                                entry[group_name] = int(m.group(group_name))
                            except (ValueError, TypeError):
                                entry[group_name] = m.group(group_name)
                        level_zero_outputs.append(entry)

        if len(level_zero_outputs) > 0:
            self.logger.debug(msg=f"Level 0: Found {len(level_zero_outputs)} matches with named groups.")
        else:
            self.logger.debug(msg=f"Level 0: None of the patterns matched. {patterns[-1]}")
        return level_zero_outputs

    def level_one(self, text, command):
        level_zero_outputs = self.level_zero(text=text, patterns=self.patterns["level0"][command])
        level_one_outputs = []
        if command not in self.patterns["level1"].keys():
            return level_one_outputs
        if len(level_zero_outputs) == 0:
            self.logger.error(msg=f"Level 1: Level 0 returned 0 outputs.")
            return []
        else:
            if isinstance(level_zero_outputs[0], dict):
                for level_zero_entry in level_zero_outputs:
                    new_entry = level_zero_entry
                    for key, patterns in self.patterns["level1"][command].items():
                        try:
                            new_entry[key] = self.level_zero(text=level_zero_entry[key], patterns=patterns)
                        except TypeError:
                            new_entry[key] = []
                    level_one_outputs.append(new_entry)
            if isinstance(level_zero_outputs[0], str):
                for level_zero_entry in level_zero_outputs:
                    new_entry = {}
                    for key, patterns in self.patterns["level1"][command].items():
                        for out in self.level_zero(text=level_zero_entry, patterns=patterns):
                            new_entry.update(out)
                    level_one_outputs.append(new_entry)
        return level_one_outputs

    def command_mapping(self, command):
        levels = ["level0", "level1"]
        max_level = None
        for level in levels:
            if command in list(self.patterns[level].keys()):
                max_level = level
        self.logger.debug(msg=f"Command '{command}' level is: {max_level}")
        return max_level

    def autoparse(self, text, command):
        command_level = self.command_mapping(command=command)
        parsed_output = None
        if command_level == "level0":
            return self.level_zero(text=text, patterns=self.patterns["level0"][command])
        elif command_level == "level1":
            return self.level_one(text=text, command=command)
        else:
            self.logger.critical(msg=f"AutoParse: Unknown level for command: '{command}'")
