import os
import sys
import json
from cisco_facts_gathering.utils import get_logger, check_path
from cisco_facts_gathering.definitions import ROOT_DIR, DATA_PATH


class Writer:
    def __init__(self, type, DEBUG=False):
        self.type = type
        self.logger = get_logger(name=f"{self.type}-Writer", DEBUG=DEBUG)

    def __str__(self):
        return f"[{self.type}-Writer]"

    def __repr__(self):
        return f"[{self.type}-Writer]"

    def json_to_lists(self, data):
        headers = []
        list_data = []
        if isinstance(data, list) and len(data) > 0:
            if isinstance(data[0], dict):
                headers = list(data[0].keys())
                self.logger.debug(msg=f"Successfully created headers: {headers}")
                for entry in data:
                    entry_list = []
                    for element in [entry[x] for x in headers]:
                        if isinstance(element, list):
                            entry_list.append(str(element))
                        else:
                            entry_list.append(element)
                    list_data.append(entry_list)
                return {"headers": headers, "list_data": list_data}


        elif isinstance(data, str):
            self.logger.warning(msg=f"Given data is a string. List of dictionaries is preferred.")
            try:
                data = json.loads(data)
                return self.json_to_lists(data=data)
            except:
                self.logger.critical(msg=f"Given data is not a valid JSON string.")
                return None

        pass
