from cisco_facts_gathering.Writers import Writer
from cisco_facts_gathering.utils import check_path
from cisco_facts_gathering.definitions import DATA_PATH
import xlsxwriter

class ExcelWriter(Writer):
    def __init__(self):
        super(ExcelWriter, self).__init__(type="Excel")

    def create_workbook(self, path, filename):
        path = check_path(path)
        workbook = xlsxwriter.Workbook(f"{path}/{filename}")
        return workbook

    def write_json(self, workbook, data, worksheetname=None):
        if isinstance(data, list):
            # Generate headers from first dict
            headers = list(data[0].keys())
            bold = workbook.add_format({"bold": True})
            # Create new worksheet
            worksheet = None
            if worksheetname:
                self.logger.info(msg=f"Creating new worksheet with name {worksheetname}.")
                worksheet = workbook.add_worksheet(name=worksheetname)
            else:
                self.logger.info(msg="Creating new worksheet.")
                worksheet = workbook.add_worksheet()
            row_pointer = 0
            column_pointer = 0
            worksheet.write_row(row_pointer, column_pointer, headers, bold)
            row_pointer += 1
            for entry in data:
                row = []
                # Workaround for unsupported types
                for element in [entry[x] for x in headers]:
                    if isinstance(element, list):
                        row.append(str(element))
                    else:
                        row.append(element)
                worksheet.write_row(row_pointer, column_pointer, row)
                row_pointer += 1
            self.logger.info(msg=f"{len(data)} entries writen.")

        else:
            # TODO: Implement other JSON types
            self.logger.error(msg=f"Given JSON is not a list of dictionaries. Not yet implemented.")

if __name__ == '__main__':

    xls = ExcelWriter()
    workbook = xls.create_workbook(path=f"{DATA_PATH}/XLS", filename="test.xlsx")
    xls.write_json(workbook, [{"A": 1, "B": 2}])
    workbook.close()