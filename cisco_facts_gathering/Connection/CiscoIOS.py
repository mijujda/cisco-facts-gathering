from netmiko import ConnectHandler
from netmiko.ssh_exception import *

import sys
import os
import json
import datetime
import logging
import time
from cisco_facts_gathering.utils import get_logger, check_path
from cisco_facts_gathering.utils import Filter
from cisco_facts_gathering.definitions import DATA_PATH
from cisco_facts_gathering.Parsers import CiscoIOSParser
from cisco_facts_gathering.Writers import ExcelWriter
from paramiko.ssh_exception import SSHException, AuthenticationException
from socket import error


class CiscoIOS(object):

    def __init__(self, ip=None, username=None, password=None, device_type=None, parser=None, secret=None, enable=False, store_outputs=False ,DEBUG=False):

        self.ip = ip
        self.username = username
        self.password = password
        self.device_type = device_type if device_type else "cisco_ios"
        self.secret = secret
        self.enable = enable
        self.store_outputs = store_outputs
        self.enabled = None
        self.logger = get_logger(f"Connection-{self.ip}", DEBUG=DEBUG)
        self.parser = parser if isinstance(parser, CiscoIOSParser) else CiscoIOSParser()
        self.connected = False
        self.provider = None
        self._get_provider()
        self.outputs = {}
        self.data = {}
        self.device = None

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.store_raw_output("JSON", json.dumps(self.data, indent=2))
        self.disconnect()


    def _get_provider(self):
        self.provider = {
            "device_type": self.device_type,
            "ip": self.ip,
            "username": self.username,
            "password": self.password
        }
        if self.secret:
            self.provider["secret"] = self.secret

    def connect(self):
        # Disable traceback output because of Paramiko threaded exceptions
        sys.tracebacklimit = 0
        device = None
        self.logger.debug(msg=f"Trying to connect to device {self.ip}...")
        try:
            device = ConnectHandler(**self.provider)
        except (error, EOFError, SSHException):
            self.logger.error(msg=f"Device may not support {self.device_type} connection method.")
            try:
                if self.device_type == "cisco_ios":
                    self.device_type = "cisco_ios_telnet"
                    self.logger.info(msg=f"Trying Telnet instead.")
                elif self.device_type == "cisco_ios_telnet":
                    self.device_type = "cisco_ios"
                    self.logger.info(msg=f"Trying SSH instead.")
                self._get_provider()
                print(self.provider)
                device = ConnectHandler(**self.provider)
            except NetMikoTimeoutException:
                print("Unable to connect.")
            except TimeoutError:
                print("TimeOut")

        except NetMikoTimeoutException:
            print("TimeOutException")
        except (NetMikoAuthenticationException, AuthenticationException):
            print("Authentication Exception. Wrong credentials?")
        # Re-enable traceback output
        sys.tracebacklimit = 1

        if device is not None:
            prompt = device.find_prompt()
            if prompt[-1] == "#":
                self.enabled = True
            elif prompt[-1] == ">":
                self.enabled = False
            self.logger.debug(msg=f"Sucessfully connected to device {prompt[:-1]} ({self.ip}). Privileged EXEC: {self.enabled}")
            self.device = device
            self.data["hostname"] = prompt[:-1]

    def disconnect(self):
        if self.device is not None:
            self.device.disconnect()
            if not self.device.is_alive():
                self.logger.info(msg=f"Sucessfully disconnected from device {self.ip}")
            else:
                self.logger.error(msg=f"Failed to disconnect from device {self.ip}")
        else:
            self.logger.info(msg=f"Device {self.ip} is not connected.")

    def _send_command(self, command):
        if not self.device:
            self.logger.error(msg=f"Device {self.ip} is not connected, cannot send command.")
            return None
        self.logger.debug(msg=f"Sending command '{command}' to device {self.data['hostname']} ({self.ip})")
        output = ""
        try:
            output = self.device.send_command_expect(command)
        except AttributeError:
            self.logger.critical(msg=f"Connection to device {self.ip} has not been initialized.")
        finally:
            return output

    def _send_commands(self, commands):
        output = {}
        for command in commands:
            output[command] = self._send_command(command)
        return output

    def get_neighbors(self, output_filter=None, strip_domain=False):
        command = "show cdp neighbors detail"
        raw_output = self._send_command(command=command)
        if self.store_outputs:
            self.store_raw_output(command=command, raw_output=raw_output)
        parsed_output = self.parser.autoparse(text=raw_output, command=command)
        if output_filter:
            parsed_output = output_filter.universal_cleanup(data=parsed_output)
        if strip_domain:
            for neighbor in parsed_output:
                neighbor["hostname"] = neighbor["hostname"].split(".")[0]
        self.data["neighbors"] = parsed_output
        return parsed_output

    def get_vlans(self):
        command = "show vlan brief"
        raw_output = self._send_command(command=command)
        if self.store_outputs:
            self.store_raw_output(command=command, raw_output=raw_output)
        parsed_output = self.parser.autoparse(text=raw_output, command=command)
        self.data["vlans"] = parsed_output
        return parsed_output

    def get_inventory(self):
        command = "show inventory"
        raw_output = self._send_command(command=command)
        if self.store_outputs:
            self.store_raw_output(command=command, raw_output=raw_output)
        parsed_output = self.parser.autoparse(text=raw_output, command=command)
        self.data["inventory"] = parsed_output
        return parsed_output

    def get_interfaces(self):
        command = "show interfaces"
        raw_output = self._send_command(command=command)
        if self.store_outputs:
            self.store_raw_output(command, raw_output)
        parsed_output = self.parser.autoparse(text=raw_output, command=command)
        self.data["interfaces_detail"] = parsed_output
        return parsed_output

    def get_portchannels(self):
        command = "show etherchannel summary"
        raw_output = self._send_command(command=command)
        print(raw_output)
        if self.store_outputs:
            self.store_raw_output(command=command, raw_output=raw_output)
        parsed_output = self.parser.autoparse(text=raw_output, command=command)
        self.data["portchannels"] = raw_output
        return parsed_output

    def get_version(self):
        command = "show version"
        raw_output = self._send_command(command=command)
        if self.store_outputs:
            self.store_raw_output(command=command, raw_output=raw_output)

    def get_mac_addresses(self):
        command = "show mac address-table"
        raw_output = self._send_command(command=command)
        if self.store_outputs:
            self.store_raw_output(command=command, raw_output=raw_output)
        parsed_output = self.parser.autoparse(text=raw_output, command=command)
        self.data["macAddressTable"] = parsed_output
        return parsed_output

    def get_arp(self, vrf=None):
        if vrf:
            # TODO: Implement VRF Checking
            pass
        else:
            command = "show ip arp"
            raw_output = self._send_command(command=command)
            if self.store_outputs:
                self.store_raw_output(command=command, raw_output=raw_output)
            parsed_output = self.parser.autoparse(text=raw_output, command=command)
            self.data["arpTable"] = parsed_output
            return parsed_output


    def store_raw_output(self, command, raw_output):
        path = f"{DATA_PATH}/outputs/{self.ip}"
        path = check_path(path)
        if path:
            with open(f"{path}/{command}.txt", mode="w+") as f:
                f.write(raw_output)

    def __str__(self):
        return f"[Connection -> {self.ip}]"

    def __repr__(self):
        return f"[Connection -> {self.ip}]"

if __name__ == '__main__':
    provider = {
        "ip": "10.2.1.155",
        "device_type": "cisco_ios",
        "store_outputs": True,
        "DEBUG": True
    }
    with open(f"{DATA_PATH}/credentials.json", mode="r") as creds:
        provider.update(json.load(creds))

    with CiscoIOS(**provider) as device:
        print(json.dumps(device.get_arp(), indent=2))
