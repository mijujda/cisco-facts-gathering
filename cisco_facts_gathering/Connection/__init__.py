import logging
# Disable error logging for Paramiko library
logging.getLogger("paramiko").setLevel(logging.CRITICAL)
from cisco_facts_gathering.Connection.CiscoIOS import CiscoIOS
