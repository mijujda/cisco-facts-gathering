from cisco_facts_gathering.Connection import CiscoIOS
from cisco_facts_gathering.utils import get_logger
from cisco_facts_gathering.utils import Filter
import json
class CDPDiscovery:
    def __init__(self, provider, DEBUG=False):
        self.DEBUG = DEBUG
        self.logger = get_logger(name="CDPDiscovery", DEBUG=self.DEBUG)
        self.provider = provider
        self.inventory = {}
        self.topology = {"nodes": {}, "links": []}
        self.current_id = 0
        self.neighbor_filter = Filter(required={"capabilities": ["Router", "Switch"]}, exact_match=False)

    def discover_device(self, ip, hostname=None):
        self.logger.debug(msg=f"Discovering device {ip}")
        device = CiscoIOS(ip=ip, **self.provider, DEBUG=self.DEBUG)
        device.connect()
        if not device.device:
            if hostname:
                self.inventory[hostname] = {"status": "failed", "ipAddress": ip, "id": self.current_id}
            else:
                self.inventory[ip] = {"status": "failed", "ipAddress": ip, "id": self.current_id}
            self.current_id += 1
            self.logger.error(msg=f"Could not connect to device {ip}.")
        else:
            device_hostname = device.data["hostname"]
            device_neighbors = device.get_neighbors(output_filter=self.neighbor_filter, strip_domain=True)
            device.disconnect()
            if device_hostname in list(self.inventory.keys()):
                # If device appeared as a neighbor before
                self.logger.debug(msg=f"Device {device_hostname} ({ip}) has been seen before, status changed to 'completed'.")
                self.inventory[device_hostname]["status"] = "completed"
            else:
                # If this device is seen for the first time (eg. starting device)
                self.logger.debug(msg=f"Starting device {device_hostname} ({ip}), status changed to 'completed'.")
                self.inventory[device_hostname] = {"status": "completed", "ipAddress": ip, "id": self.current_id}
                self.topology["nodes"][self.current_id] = {"hostname": device_hostname}
                self.current_id += 1
            for neighbor in device_neighbors:
                # Add neighbor to inventory and topology[nodes]
                if neighbor["hostname"] not in list(self.inventory.keys()):
                    self.inventory[neighbor["hostname"]] = {"status": "pending", "ipAddress": neighbor["ipAddress"], "id": self.current_id}
                    self.topology["nodes"][self.current_id] = {"hostname": neighbor["hostname"]}
                    self.current_id += 1
                # If the neighbor is in "pending" status, add links
                if self.inventory[neighbor["hostname"]]["status"] != "completed":
                    self.logger.debug(msg=f"Neighbor {neighbor['hostname']} ({neighbor['ipAddress']}) Status: '{self.inventory[neighbor['hostname']]['status']}', adding links.")
                    self.topology["links"].append({
                        "source": self.inventory[device_hostname]["id"],
                        "target": self.inventory[neighbor["hostname"]]["id"],
                        "sourceInterface": neighbor["localInterface"],
                        "targetInterface": neighbor["remoteInterface"],
                        "sourceHostname": device_hostname,
                        "targetHostname": neighbor["hostname"]
                    })
                else:
                    self.logger.debug(msg=f"Neighbor {neighbor['hostname']} ({neighbor['ipAddress']}) already has Status: 'completed', not adding links.")

    def auto_discovery(self, start_ip, max_devices=100):
        # Discover starting device
        self.discover_device(ip=start_ip)
        pending_devices = []
        for hostname, data in self.inventory.items():
            if data["status"] == "pending":
                pending_devices.append(data["ipAddress"])
        discovered_devices = 1
        while len(pending_devices) > 0 and discovered_devices <= max_devices:
            try:
                print(pending_devices)
                self.discover_device(ip=pending_devices[0])
                pending_devices = pending_devices[1:]
                for hostname, data in self.inventory.items():
                    if data["status"] == "pending" and data["ipAddress"] not in pending_devices:
                        pending_devices.append(data["ipAddress"])
            except KeyboardInterrupt:
                self.logger.info(msg="KeyboardInterrupt")
                break
