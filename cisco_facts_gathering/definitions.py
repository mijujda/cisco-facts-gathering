import os
import sys

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_PATH = os.path.join(ROOT_DIR, "Data")
LOG_PATH = os.path.join(DATA_PATH, "logs")
